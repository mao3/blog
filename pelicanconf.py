#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'mao'
SITENAME = 'mao'
SITEURL = 'https://gitlab.com/mao3/blog'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/Los_Angeles'
DEFAULT_LANG = 'en'

SLUGIFY_SOURCE = 'title'
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

MENUITEMS = (
    ('mao', 'index.html'),
    ('self', 'self.html'),
    ('code', 'https://gitlab.com/mao3')
)

PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
ARTICLE_URL = '{slug}.html'
ARTICLE_SAVE_AS = '{slug}.html'
CATEGORY_SAVE_AS = ''
TAG_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
DIRECT_TEMPLATES = ['index']

USE_FOLDER_AS_CATEGORY = True

FEED_RSS = None
FEED_ALL_RSS = None
FEED_ATOM = None
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
DEFAULT_PAGINATION = False
LINKS = None
SOCIAL = None

THEME = "./mao"

RELATIVE_URLS = False
