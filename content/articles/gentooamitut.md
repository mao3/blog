Title: gentoo ami tut
Date: 2016-07-15 08:13


###### 160715

<br>
## host.instance
<br>

launch an ec2 instance, 8gb wtih a second 8gb volume as /dev/sdb

\### i used an ubuntu ami

<br>
ssh in to new instance

sudo bash


<br><br>
## partition.disk
<br>

lsblk

parted /dev/xvdb

unit s

mklabel msdos

mkpart primary 4096 6G

mkpart primary 11718656s 100%

print

quit


<br><br>
## create.fs & mounts
<br>

mkfs.ext4 /dev/xvdb1

e2label /dev/xvdb1 temp-rootfs

mkswap --label swap /dev/xvdb2

<br>
mkdir -p /mnt/gentoo

mount /dev/xvdb1 /mnt/gentoo

swapon /dev/xvdb2


<br><br>
## download.stage3
<br>

cd /mnt/gentoo

<br>
wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened/stage3-amd64-hardened+nomultilib-20160630.tar.bz2

wget http://distfiles.gentoo.org/releases/snapshots/current/portage-latest.tar.xz

<br>
tar xvpf stage3-\*.tar.bz2

tar xvf portage-latest.tar.xz -C /mnt/gentoo/usr/


<br><br>
## chroot
<br>

cp /etc/resolve.conf /mnt/gentoo/etc/

<br>
mount -t proc none /mnt/gentoo/proc

mount -o bind /dev /mnt/gentoo/dev

<br>
chroot /mnt/gentoo /bin/bash

env-update && source /etc/profile


<br><br>
## configs
<br>

nano /etc/portage/make.conf

<br>
nano /etc/rc.conf

<br>
eselect profile list

eselect profile set X

<br>
nano /etc/locale.gen

locale-gen

eselect locale list

eselect locale set X

<br>
rc-update delete keymaps boot

<br>
ln -sf /usr/share/zoneinfo/US/Pacific /etc/localtime

echo "US/Pacific" > /etc/timezone

<br>
nano /etc/conf.d/hostname

nano /etc/hosts

<br>
emerge dhcpcd

nano /etc/conf.d/net

ln -s /etc/init.d/net.lo /etc/init.d/net.eth0

rc-update add net.eth0 default


<br><br>
## kernel
<br>

emerge hardened-sources

<br>
cd /usr/src/linux

make menuconfig

<br>
make && make modules_install

make install


<br><br>
## gen.initramfs
<br>

emerge genkernel

genkernel --install initramfs

<br><br>
## cloud.init
<br>

passwd -d -l root

rc-update add sshd default

<br>
echo "app-admin/amazon-ec2-init" >> /etc/portage/package.accept_keywords

emerge app-admin/amazon-ec2-init

rc-update add amazon-ec2 boot


<br><br>
## grub.stuffs
<br>

emerge grub

<br>
exit # exit.chroot

<br>
cp /mnt/gentoo/boot/initram\* /boot/

cp /mnt/gentoo/boot/bz\* /boot/

cp /mnt/gentoo/boot/System.map\* /boot/

<br>
nano /boot/grub/grub.cfg

<br>
reboot


<br><br>
## log back in to instance
<br>

vim .ssh/knownhosts # rm ip

ssh back in as root # !ubuntu

<br><br>
## clone.gentoo.to.first.vol
<br>

mkdir /mnt/gentoo && mount /dev/xvdb1 /mnt/gentoo

mkfs.ext4 /dev/xvda1

e2label /dev/xvda1 cloudimg-rootfs

mkdir -p /mnt/gentoo

mount /dev/xvda1 /mnt/gentoo

<br>
mkdir /mnt/gentoo/run

mkdir /mnt/gentoo/proc

mkdir /mnt/gentoo/mnt

touch /mnt/gentoo/mnt/.keep

mkdir /mnt/gentoo/sys

mkdir /mnt/gentoo/tmp -m 1777

cp -rp /bin /boot /dev /etc /home /lib /lib64 /media /opt /root /sbin /usr /var /mnt/gentoo


<br><br>
## chroot
<br>

mount -t proc none /mnt/gentoo/proc

mount -o bind /dev /mnt/gentoo/dev

chroot /mnt/gentoo /bin/bash

env-update && source /etc/profile


<br><br>
## grub.install
<br>

grub2-install /dev/xvda

nano /etc/default/grub

grub2-mkconfig -o /boot/grub/grub.cfg

<br>
nano /etc/fstab

<br>
reboot


<br><br>
## cleanup
<br>

ssh back into instance as root

<br>
nano /etc/conf.d/hostname

nano /etc/hosts


<br><br>
## update && install.things
<br>

emerge --sync

emerge --update --newuse --deep @world

emerge tmux vim syslog-ng logrotate

<br>
touch /forcefsck

reboot

<br>
ssh back into instance as root

<br>
nano /etc/conf.d/hostname

exit # kill.ssh.session


<br><br>
## create.ami
<br>

detach sdb from volumes

create snapshot

create image


<br><br>
## test
<br>

launch a new instance with the new ami

<br>
## HOORAY !!!
