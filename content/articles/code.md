Title: code snippets
Date: 2020-09-23
Author: mao
Slug: code-example



###### 200923

```python
#!/bin/python
    
    def hello():
        print('hello, world')

    hello()
```

<br>

```c
#include <stdio.h>

    int main() {
        printf("hello, world');
        return 0;
    }
```

<br>


\-mao
