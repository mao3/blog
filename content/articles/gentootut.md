Title: gentoo install tut
Date: 2016-02-22 10:47

###### 160222

<br><br>
## net
<br>
 
ifconfig -a

ping -c 3 www.google.com


<br><br>
## disk
<br>

sgdisk /dev/sda --zap-all

fdisk /dev/sda

mkswap /dev/sda1 && swapon /dev/sda1

mkfs.ext4 /dev/sda2

mount /dev/sda2 /mnt/gentoo


<br><br>
## stage3
<br>
  
date \# UTC - MMDDhhmmYYYY

<br>
cd /mnt/gentoo


wget http://distfiles.gentoo.org/releases/x86/autobuilds/20160216/stage3-i686-20160216.tar.bz2

tar xvjpf stage3-\*.tar.bz2 --xattrs


nano /mnt/gentoo/etc/portage/make.conf


mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf


mkdir /mnt/gentoo/etc/portage/repos.conf

cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf


<br><br>
## chroot
<br>

cp -L /etc/resolv.conf /mnt/gentoo/etc/

<br>
mount -t proc proc /mnt/gentoo/proc

mount --rbind /sys /mnt/gentoo/sys

mount --rbind /dev /mnt/gentoo/dev

chroot /mnt/gentoo /bin/bash

source /etc/profile

export PS1="(chroot) $PS1"


<br><br>
## sync && conf 
<br>

emerge-webrsync

emerge --sync

<br>
eselect profile list

eselect profile set 1

<br>
emerge --info | grep ^USE

less /usr/portage/profile/use.desc

nano /etc/portage/make.conf

<br>
ls /usr/share/zoneinfo/America/Los_Angeles

echo "America/Los_Angeles" > /etc/timezone

emerge --config sys-libs/timezone-data

<br>
nano /etc/locale.gen

locale-gen

locale -a

eselect locale list

eselect locale set 3

env-update && source /etc/profile && export PS1="(chroot) $PS1"

  
<br><br>
## kernel
<br>

emerge --ask sys-kernel/gentoo-sources

emerge --ask sys-apps/pciutils

lspci

<br>  
cd /usr/src/linux

make menuconfig

make && make modules_install

make install

<br>
emerge --ask sys-kernel/genkernel

genkernel --install initramfs

<br>  
find /lib/modules/<kernel version>/ -type f -iname '\*.o' -or -iname '\*.ko' | less

nano /etc/conf.d/modules

<br>
emerge --ask sys-kernel/linux-firmware


<br><br>
## conf
<br>

nano /etc/fstab

<br>  
nano /etc/conf.d/hostname

<br>  
emerge --ask --noreplace net-misc/netifrc

nano /etc/conf.d/net # config_enp2s0="dhcp"

<br>  
cd /etc/init.d

ln -s net.lo net.enp2s0

rc-update add net.enp2s0 default

<br>
nano /etc/hosts

<br>  
emerge --ask sys-apps/pcmciautils

<br>
passwd

<br>
nano /etc/rc.conf

nano /etc/conf.d/keymaps

nano /etc/conf.d/hwclock  


<br><br>
## install.things
<br>

emerge --ask app-admin/sysklogd

rc-update add sysklogd default

emerge --ask sys-process/cronie

rc-update add cronie default

emerge --ask sys-apps/mlocate

rc-update add sshd default

emerge --ask net-misc/dhcpcd

<br>
emerge --ask sys-boot/grub

grub2-install /dev/sda

grub2-mkconfig -o /boot/grub/grub.cfg

<br>
exit

cd ~

umount -l /mnt/gentoo

reboot \# pull media


<br><br>
## useradd && grub
<br>

login root

useradd -m -G users,wheel,audio -s /bin/bash yourUserName

passwd yourUserName

<br>
rm /stage3-\*.tar.bz2\*

nano /etc/default/grub \# video=1680x1050-32@60

grub2-mkconfig -o /boot/grub/grub.cfg


<br><br>
## update && install.things
<br>

emerge --sync

emerge --update --deep @world


<br>
\#### mini.emerge.cheatsheet 

\#  emerge --sync  \# sync portage

\#  emerge --update --deep @world  \# update && deps

\#  emerge --update --newuse --deep @world  \# if change useflags

\#  emerge --search <query>  \# search

\#  emerge --searchdesc <query>  \# search descriptions

\#  emerge --ask <pkg>  \# ask about deps

\#  emerge --pretend <pkg>  \# find out reqs

\#  emerge --unmerge <pkg>  \# uninstall pkg

\####

<br>
    
emerge vim tree mutt lynx git irssi htop 


<br><br>
## rm.systemd
<br>

vim /etc/portage/make.conf \# -systemd

vim /etc/portage/package.mask/systemd 

emerge --ask eudev


<br><br>
## misc.conf
<br>

vim /etc/issue

