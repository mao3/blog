Title: mnml lfs cyoa tut
Date: 2016-07-15 19:41


###### 160715

<br>
\# mnml.lfs(busybox||toybox||sbase+ubase)

<br>
\### format && mount partition

\### compile kernel && static binaries

\### create symlinks && init && initrd

\### boot

<br>
\#### i built these locally on metal, booted them from the grub cli

\#### my build system was crux 3.2 with gcc 5.4.0

\#### buildbox << dumpstered toshiba with an i3 and spare partitions

\#### the goal was to boot to mksh entirely from source

<br>
\# cyoa! << busybox||toybox||sbase+ubase ; GO! 


<br><br>
## disk.format && disk.mount 
<br>

su  \# be root 

mkfs.ext4 /dev/X  \# X=partition.num

mount /dev/X /mnt/whatever

cd /mnt/whatever


<br><br>
## mk.kernel
<br>

wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.6.3.tar.xz

<br>
tar xvf linux-4.6.3.tar.xz && cd linux-4.6.3

make menuconfig

make 

cp arch/x86_64/boot/bzImage ../

cd ..

rm linux-4.6.3.tar.xz


<br><br>
### busybox-1.25.0
<br>


wget https:/www.busybox.net/downloads/busybox-1.25.0.tar.bz2

<br>
tar xvjf busybox-1.25.0.tar.bz2 && cd busybox-1.25.0

make menuconfig

make && make install

<br>
mv \_install ../rootfs

cd ../rootfs

mkdir dev proc sys tmp

rm linuxrc

mknod dev/console c 5 1

cd ..

rm busybox-1.25.0.tar.b2


<br><br>
### toybox-0.7.1
<br>

wget http://www.landley.net/toybox/downloads/toybox-0.7.1.tar.gz

<br>
mkdir rootfs && cd rootfs

mkdir bin dev mnt proc sys sbin tmp usr usr/bin

mknod dev/console c 5 1

cd ..

<br>
tar xvf toybox-0.7.1.tar.gz && cd toybox-0.7.1

make menuconfig

make

<br>
cp toybox ../rootfs/bin

cd ../rootfs/bin

for i in $(toybox --long); do ln -s toybox $i; done

cd ../../

rm toybox-0.7.1.tar.gz


<br><br>
### sbase+ubase
<br>

git clone git://git.suckless.org/sbase

git clone git://git.suckless.org/ubase

<br>
mkdir rootfs && cd rootfs

mkdir bin dev mnt proc sys sbin tmp usr usr/bin

mknod dev/console c 5 1

cd ..

<br>
cd sbase

make LDFLAGS="-s -static" sbase-box

cp sbase-box ../rootfs/bin/sbase

cd ../rootfs/bin

for i in $(sbase); do ln -s sbase $i; done

cd ../..

<br>
cd ubase

make LDFLAGS="-s -static" ubase-box

cp ubase-box ../rootfs/bin/ubase

cd ../rootfs/bin

for i in $(ubase); do ln -s ubase $i; done

cd ../..


<br><br>
## mk.mksh
<br>

wget https://www.mirbsd.org/MirOS/dist/mir/mksh/mksh-R52c.tgz

<br>
tar xvf mksh-R52c.tgz && cd mksh

LDSTATIC=-static ./Build.sh

cp mksh ../rootfs/bin

ln -s mksh sh

cd ..

rm mksh-R52c.tgz


<br><br>
## mk.init
<br>


\#### mksh is optional for busybox, it has it's own sh.

\#### just launch /bin/sh instead of mksh

<br>
cat >> init << EOF

\#!/bin/sh

dmesg -n 1

mount -t devtmpfs dev /dev

mount -t proc none /proc

mount -t sysfs none /sys

while true; do /bin/mksh; done 

EOF

<br>
chmod +x init


<br><br>
## mk.initrd
<br>


find . | cpio -H newc -o | gzip > ../rootfs.cpio.gz


<br><br>
## grub.cli
<br>

set root=(hd0,X)  \# X=partition.num

linux /bzImage

initrd /rootfs.cpio.gz

boot


<br><br>
\# you should be looking at a shell now


<br>
\### YAY !!! YOU DID IT !!!

